defmodule DeliveryCenter.Repo.Migrations.CreateOrderItems do
  use Ecto.Migration

  def change do
    create table(:order_items) do
      add :item, :map
      add :quantity, :integer
      add :unit_price, :decimal
      add :full_unit_price, :decimal
      add :order_id, references(:orders, on_delete: :nothing)

      timestamps()
    end

    create index(:order_items, [:order_id])
  end
end
