defmodule DeliveryCenter.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :store_id, :integer
      add :date_created, :naive_datetime
      add :date_closed, :naive_datetime
      add :last_updated, :naive_datetime
      add :total_amount, :decimal
      add :total_shipping, :decimal
      add :total_amount_with_shipping, :decimal
      add :paid_amount, :decimal
      add :expiration_date, :naive_datetime
      add :status, :string

      timestamps()
    end

  end
end
