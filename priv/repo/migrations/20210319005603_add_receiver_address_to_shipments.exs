defmodule DeliveryCenter.Repo.Migrations.AddReceiverAddressToShipments do
  use Ecto.Migration

  def change do
    alter table(:shipments) do
      add :receiver_address, references(:addresses, on_delete: :nothing)
    end

  end

end
