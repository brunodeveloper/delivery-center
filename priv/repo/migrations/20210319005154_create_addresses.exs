defmodule DeliveryCenter.Repo.Migrations.CreateAddresses do
  use Ecto.Migration

  def change do
    create table(:addresses) do
      add :address_line, :string
      add :street_name, :string
      add :street_number, :string
      add :comment, :string
      add :zip_code, :string
      add :city, :map
      add :state, :map
      add :country, :map
      add :neighborhood, :map
      add :latitude, :decimal
      add :longitude, :decimal
      add :receiver_phone, :string

      timestamps()
    end

  end
end
