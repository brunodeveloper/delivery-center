defmodule DeliveryCenter.Repo.Migrations.CreateBuyers do
  use Ecto.Migration

  def change do
    create table(:buyers) do
      add :nickname, :string
      add :email, :string
      add :phone, :map
      add :first_name, :string
      add :last_name, :string
      add :billing_info, :map
      add :order_id, references(:orders, on_delete: :nothing)

      timestamps()
    end
  end
end
