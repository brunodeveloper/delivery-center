defmodule DeliveryCenter.Repo.Migrations.CreateShipments do
  use Ecto.Migration

  def change do
    create table(:shipments) do
      add :shipment_type, :string
      add :date_created, :naive_datetime
      add :order_id, references(:orders, on_delete: :nothing)

      timestamps()
    end

  end
end
