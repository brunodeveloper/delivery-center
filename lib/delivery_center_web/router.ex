defmodule DeliveryCenterWeb.Router do
  use DeliveryCenterWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", DeliveryCenterWeb do
    pipe_through :api
  end
end
