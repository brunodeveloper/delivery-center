# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :delivery_center,
  ecto_repos: [DeliveryCenter.Repo]

# Configures the endpoint
config :delivery_center, DeliveryCenterWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "KoncAND4a48EDU6BDCxOb819Pgj6KK5pDudLV4D5f5wmNVMHd9rCwgJyKgZiFitm",
  render_errors: [view: DeliveryCenterWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: DeliveryCenter.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
